This project aims to change Evennia so that all input is displayed as input to the say command unless a specific character precedes in, like IRC. This is done in such a way that except for the inclusion of two files and some custom setting entries Evennia continues unchanged.

The files of interest are:

server/conf/settings - Where the command character and command exclusions are set.

commands/IRCparser - A custom parser that handles expecting the command character.

commands/IRCcmd - Extending the say command to be the CMD_NOMATCH