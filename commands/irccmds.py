"""
IRC Parser

Contribution - Cloud_Keeper 2016

Evennia input will behave closer to IRC standard.
All input will be sent as a "say" message.
All commands must be preceded with a command character - "/" by default.

Example:
>Hello there.
You say, "Hello there."
>look
You say, "look"
>/look
Limbo(#2)
Welcome to your new Eve...

The system works by automatically returning input strings which are not
preceded by the command character as having no matches. We tell Evennia to
send all non matching inputs to our amended Say command with the CMD_NOMATCH
alias, which then treats the input as "say" input.

To use:

In mygame/server/conf/settings paste:
COMMAND_PARSER = "evennia.contrib.irc_parser.ircparser.cmdparser"
COMMAND_STRING = "/"
COMMAND_EXCEPTIONS = ["__unloggedin_look_command"]

In mygame/commands/default_cmdsets.py:
from evennia.contrib.irc_parser.irccmds import CmdSay

class CharacterCmdSet(default_cmds.CharacterCmdSet):
        ...
        self.add(CmdSay())
"""
from django.conf import settings
from evennia.utils import utils
from evennia.commands import cmdhandler
from evennia.commands.default.general import CmdSay
from evennia.utils.utils import string_suggestions
from evennia.commands.command import Command


class CmdParserSay(Command):
    """
    speak as your character -extended to be the default no match option.

    Usage:
      say <message>

    Talk to those in your current location.
    """

    key = cmdhandler.CMD_NOMATCH

    def func(self):
        """Capture incorrect commands otherwise run the say command"""
        if self.args[0] == settings.COMMAND_STRING:
            # Incorrect command. String had command character but didn't match
            # any commands. Fallback to default error text
            string = self.args[1:]
            sysarg = ("Command '%s' is not available.") % string
            suggestions = string_suggestions(string, self.cmdset.get_all_cmd_keys_and_aliases(self.caller),
                                             cutoff=0.7, maxnum=3)
            if suggestions:
                sysarg += (" Maybe you meant %s?") % utils.list_to_string(suggestions, ('or'), addquote=True)
            self.caller.msg(sysarg)
            return

        # Trigger the say command so it can fail if say cmd not available.
        self.caller.execute_cmd(settings.COMMAND_STRING + "say " + self.args)

def irc_input_parser(menuobject, raw_string, caller):
    """
    EvMenu supersedes the IRC Parser System by overriding commands with it's
    own CMD_NOMATCH command and handling parsing itself. This is an example
    of a modified parser that can be fed to an instance of EvMenu which
    will maintain the IRC Parser System paradigms.

    Processes the user's node inputs.

    To use:
        from evennia.contrib.irc_parser.irccmds import irc_input_parser

        EvMenu(self.caller,
               ...
               input_parser=irc_input_parser
               ...)

    Args:
        menuobject (EvMenu): The EvMenu instance
        raw_string (str): The incoming raw_string from the menu
            command.
        caller (Object, Player or Session): The entity using
            the menu.
    """
    ###########################################################################
    # This code enables the IRC Parser command character.
    ###########################################################################
    if raw_string:
        if raw_string[0] != settings.COMMAND_STRING:
            # Send to say command.
            caller.execute_cmd(settings.COMMAND_STRING + "say " + raw_string)
        # Remove command string
        raw_string = raw_string[1:]
    ###########################################################################

    cmd = raw_string.strip().lower()

    if cmd in menuobject.options:
        # this will take precedence over the default commands
        # below
        goto, callback = menuobject.options[cmd]
        menuobject.callback_goto(callback, goto, raw_string)
    elif menuobject.auto_look and cmd in ("look", "l"):
        menuobject.display_nodetext()
    elif menuobject.auto_help and cmd in ("help", "h"):
        menuobject.display_helptext()
    elif menuobject.auto_quit and cmd in ("quit", "q", "exit"):
        menuobject.close_menu()
    elif menuobject.default:
        goto, callback = menuobject.default
        menuobject.callback_goto(callback, goto, raw_string)
    else:
        caller.msg("Choose an option or try 'help'.",
                   session=menuobject._session)

    if not (menuobject.options or menuobject.default):
        # no options - we are at the end of the menu.
        menuobject.close_menu()
